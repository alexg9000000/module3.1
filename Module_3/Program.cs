﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Module_3
{
    static class Program
    {
        static void Main(string[] args)
        {
            Task1 task1 = new Task1();
            Console.Write("Введите число: ");
            string value = Console.ReadLine();
            Console.WriteLine(task1.ParseAndValidateIntegerNumber(value));
            Task2 task2 = new Task2();
            while (true)
            {
                Console.Write("Введите натуральное число: ");
                string n = Console.ReadLine();
                int number;
                if(task2.TryParseNaturalNumber(n, out number))
                {
                    Console.WriteLine(task2.GetEvenNumbers(number));
                    break;
                }
                else
                {
                    Console.WriteLine("Число не является натуральным");
                }
            }
            Task3 task3 = new Task3();
            while (true)
            {
                Console.Write("Введите натуральное число: ");
                string n = Console.ReadLine();
                int number;
                if (task3.TryParseNaturalNumber(n, out number))
                {
                    Console.Write("Введите цифру, которую нужно удалить: ");
                    int digit = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine(task3.RemoveDigitFromNumber(number, digit));
                    break;
                }
                else
                {
                    Console.WriteLine("Число не является натуральным");
                }
            }
        }
    }
    public class Task1
    {
        public int ParseAndValidateIntegerNumber(string source)
        {
            try
            {
                int num1 = int.Parse(source);
                return num1;
            }
            catch
            {
                throw new ArgumentException();
            }
        }
        public int Multiplication(int num1, int num2)
        {
            int i = 0, value = 0;
            if (num1 < 0)
            {
                while (i < -num1)
                {
                    i++;
                    value -= num2;
                }
                return value;
            }
            else
            {
                while (i < num1)
                {
                    i++;
                    value += num2;
                }
                return value;
            }
        }
    }
    public class Task2
    {
        readonly List<int> list = new List<int>();
        public bool TryParseNaturalNumber(string input, out int result)
        {
            var value = int.TryParse(input, out result);
            if(!value)
            {
                return false;
            }
            else
            {
                if(result >= 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public List<int> GetEvenNumbers(int naturalNumber)
        {
            for (int i = 0; i < naturalNumber * 2; i++)
            {
                if(i % 2 == 0)
                {
                    list.Add(i);
                }
            }
            return list;
        }
    }
    public class Task3
    {
        public bool TryParseNaturalNumber(string input, out int result)
        {
            var value = int.TryParse(input, out result);
            if (!value)
            {
                return false;
            }
            else
            {
                if (result >= 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public string RemoveDigitFromNumber(int source, int digitToRemove)
        {
            string value = Convert.ToString(source);
            string dig = Convert.ToString(digitToRemove);
            char digit = Convert.ToChar(dig);
            int i = 0;
            while (i < value.Length)
            {
                if(value[i] == digit)
                {
                    value = value.Remove(i, 1);
                }
                i++;
            }
            return value;
        }
    }
}
